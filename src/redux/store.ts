import { legacy_createStore as createStore } from 'redux'

export type State = {
  foods: {
    // id -> amount
    [id: number]: number
  }
}

// to avoid making an action in wrong shape
export function addToCart(food_id: number) {
  return {
    type: 'addToCart' as const,
    food_id,
  }
}

type Action =
  | ReturnType<typeof addToCart>
  | {
      type: 'reduceFromCart'
      food_id: number
    }

function initialState(): State {
  return {
    foods: {},
  }
}

let reducer = (state: State = initialState(), action: Action): State => {
  switch (action.type) {
    case 'addToCart': {
      let count = state.foods[action.food_id] || 0
      return {
        foods: {
          ...state.foods,
          [action.food_id]: count + 1,
        },
      }
    }
    case 'reduceFromCart': {
      let count = state.foods[action.food_id] || 0
      return {
        foods: {
          ...state.foods,
          [action.food_id]: Math.max(count - 1, 0),
        },
      }
    }
    default:
      return state
  }
}

export let store = createStore(reducer)
