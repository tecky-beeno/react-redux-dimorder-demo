import { useDispatch, useSelector } from 'react-redux'
import { addToCart, State, store } from '../redux/store'

export function CategoryList() {
  const cats = [
    {
      id: 1,
      name: 'sushi',
      foods: [
        { id: 1, name: 'fish sushi', price: 12 },
        { id: 2, name: 'tufo sushi', price: 10 },
      ],
    },
    {
      id: 2,
      name: 'rice',
      foods: [
        { id: 3, name: 'fish rice', price: 22 },
        { id: 4, name: 'tufo rice', price: 20 },
      ],
    },
  ]

  const foods = useSelector((state: State) => state.foods)
  const dispatch = useDispatch()

  function inc(food_id: number) {
    dispatch(addToCart(food_id))
  }
  function dec(food_id: number) {
    store.dispatch({
      type: 'reduceFromCart',
      food_id,
    })
  }
  return (
    <div>
      <h1>Category List</h1>
      {cats.map(cat => (
        <div key={cat.id}>
          <h2>{cat.name}</h2>
          {cat.foods.map(food => (
            <div key={food.id}>
              <h3>
                <span>{food.name}</span> <span>${food.price}</span>
              </h3>
              <button onClick={() => inc(food.id)}>+</button>
              <span>{foods[food.id] || 0}</span>
              <button onClick={() => dec(food.id)}>-</button>
            </div>
          ))}
        </div>
      ))}
    </div>
  )
}
