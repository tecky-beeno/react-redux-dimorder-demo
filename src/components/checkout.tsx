import { useSelector } from 'react-redux'
import { State } from '../redux/store'
import swal from 'sweetalert2'

export function Checkout() {
  const foods = useSelector((state: State) => state.foods)

  function submit() {
    fetch('/api/order', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(foods),
    })
      .then(res => res.json())
      .catch(error => ({ error: String(error) }))
      .then(json => {
        // delete json.error
        if (json.error) {
          // show error
          swal.fire({
            icon: 'error',
            text: json.error,
          })
          return
        }
        // show success
        swal.fire({
          icon: 'success',
          text: 'order submitted',
        })
      })
  }

  return (
    <div>
      <h1>Checkout</h1>
      {JSON.stringify(foods)}
      <button onClick={submit}>place order</button>
    </div>
  )
}
