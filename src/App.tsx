import React from 'react'
import logo from './logo.svg'
import './App.css'
import { CategoryList } from './components/category-list'
import { Checkout } from './components/checkout'

function App() {
  return (
    <div className="App">
      <CategoryList />
      <Checkout />
    </div>
  )
}

export default App
